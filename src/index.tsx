import DateRangePicker from "./DateRangePicker";
import Month from "./Month";
import moment from 'moment';
import localization from 'moment/locale/lt';

moment.updateLocale('lt', localization);
moment.locale('lt');

export { Month };
export default DateRangePicker;
